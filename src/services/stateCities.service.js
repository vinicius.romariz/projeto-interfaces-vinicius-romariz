import dataStates from "../storage/estado-municipio.json";

export const getStates = () => {
  return dataStates.estados.map(({ sigla, nome }) => ({
    sigla,
    nome,
  }));
};

export const getCities = (ufSigla) => {
  return dataStates.estados.find((item) => item.sigla === ufSigla).cidades;
};
