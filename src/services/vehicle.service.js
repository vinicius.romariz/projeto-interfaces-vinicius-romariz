import axios from "axios";
const API_FIPE = "https://parallelum.com.br/fipe/api/v1"; 

export const getBrands = async () => {
  const brands = await axios.get(API_FIPE + "/carros/marcas");
  return brands.data;
};

export const getModelFromBrand = async (brandCode) => {
  const models = await axios.get(
    API_FIPE + `/carros/marcas/${brandCode}/modelos`
  );
  return models.data.modelos;
};
