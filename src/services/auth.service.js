import http from "../config/http";

export const authenticatedUser = (values) => {
  return http.post("/auth/local", {
    identifier: values.email,
    password: values.password,
  });
};

