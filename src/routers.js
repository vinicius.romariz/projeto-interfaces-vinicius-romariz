import React from "react";
import { Router, Redirect } from "@reach/router";

// todas as views
import Portal from "./views/portal";
import Admin from "./views/admin";
import { isAuthenticated } from "./config/storage";
import Login from "./views/auth/login";


const PrivateRoute = ({ component: Component, ...rest }) => {
  if (!isAuthenticated()) {
    return <Redirect to="/login" noThrow />;
  }
  return <Component {...rest} />;
};

const Authenticated = ({ component: Component, ...rest }) => {
  if (isAuthenticated()) {
    return <Redirect to="/admin" noThrow />;
  }
 
  return <Component {...rest} />;
};



const Routers = () => {
  return (
    <Router>
      <Portal path="/*" />
      <PrivateRoute component={Admin} path="/admin/*" />
      <Authenticated component={Login} path="/login" />
    </Router>
  );
};

export default Routers;

