import { AUTH } from "../Types";

const INITIAL_STATE = {
  data: [{ id: 1, nome: "Liniker" }],
  loading: false,
  auth: {},
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AUTH.loading:
      state.loading = action.status || false;
      return state;
    case AUTH.login:
      state.loading = false;
      state.auth = action.data;
      return state;
    default:
      return state;
  }
};

export default reducer;
