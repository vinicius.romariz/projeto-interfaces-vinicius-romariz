import { navigate } from "@reach/router";
import { toast } from "react-toastify";
import http from "../../config/http";
import { saveAuth } from "../../config/storage";
import { authenticatedUser } from "../../services/auth.service";
import { AUTH } from "../Types";

export const LoginAction = (values) => {
  return async (dispatch) => {
    dispatch({ type: AUTH.loading, status: true });
    try {
      const { data } = await authenticatedUser(values);
      const dateFormat = { token: `bearer ${data.jwt}`, user: data.user };
      saveAuth(dateFormat);
      http.defaults.headers["authorization"] = dateFormat.token;
      dispatch({
        type: AUTH.login,
        data: dateFormat,
      });
      navigate("/admin");
    } catch (error) {
      toast.error("Não foi possível fazer o login, tente novamente mais tarde")
    }
  };
};
