import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import StudentReducer from "./student/reducer";
import UserReducer from "./user/reducer";


// reducers
const reducers = combineReducers({
  user: UserReducer,
  student: StudentReducer,
  
});

// Midlewares
const middlewares = [thunk];

// composetion
const compose = composeWithDevTools(applyMiddleware(...middlewares));

// store create (instancia)

const store = createStore(reducers, compose);

export default store;
