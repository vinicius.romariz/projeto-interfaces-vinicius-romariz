import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  *{
      margin:0;
      padding:0;
      outline:none;
      font-family: 'Roboto', sans-serif;
      
  }
  body,
  #root{
        height: 100vh;
        background-color:#CAF0F8;
        flex: 1;
  }

  
`;

export default GlobalStyle;

// header, footer{
//   background-color: red;
// }