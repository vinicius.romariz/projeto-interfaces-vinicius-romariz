import { navigate } from "@reach/router";
import axios from "axios";
import { getAuth, removeToken } from "./storage";

const { REACT_APP_API: api } = process.env;

const http = axios.create({
  baseURL: api,
});

http.defaults.headers["content-type"] = "application/json";
if (getAuth()) {
  const { token } = getAuth();
  http.defaults.headers["authorization"] = token;
}

http.interceptors.response.use(
  (response) => response,
  (error) => {
    switch (error.response.status) {
      case 401:
        removeToken();
        navigate("/login");
        break;
      default:
        return Promise.reject(error);
    }
  }
);

export default http; 
