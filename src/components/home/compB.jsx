import { useSelector } from "react-redux";

const CompB = () => {
  const students = useSelector((state) => state.student.data);

  return (
    <div>
      <h1>Component B</h1>
      <hr />
      <ul>
      {students?.map((item) => (
          <li key={item.id}>{item.nome}</li>
        ))}
      </ul>
    </div>
  );
};

export default CompB;
