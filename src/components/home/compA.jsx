import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import http from "../../config/http";

const CompA = () => {
  const students = useSelector((state) => state.student.data);

  useEffect(() => {
    (async () => {
      const result = await http.get("/users");
      console.log("result", result);
    })();
  }, []);

  return (
    <div>
      <h1>Component A</h1>
      <hr />
      <ul>
        {students?.map((item) => (
          <li key={item.id}>{item.nome}</li>
        ))}
      </ul>
    </div>
  );
};

export default CompA;
