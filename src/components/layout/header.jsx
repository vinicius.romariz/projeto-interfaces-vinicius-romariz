import { Container } from "reactstrap";
import styled from "styled-components";
import { IoCarSportOutline } from "react-icons/io5";

const Header = () => {
  return (
    <HeaderContainer>
      <Container> 
        <Logo>
          <Icon />
          SECURE <span>CAR</span>
        </Logo>
      </Container> 
    </HeaderContainer>
  );
};

export default Header;

const HeaderContainer = styled.div`
  padding: 20px;
  display: flex;
  justify-content: center;
  background-color: #90E0EF;
  position:fixed;
  left:0;
  top:0;
  right:0;
`;
const Logo = styled.div`
  color: ${(props) => props.theme.primary};
  font-weight: 600;
  font-size: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  span {
    color: ${(props) => props.theme.alternative};
  }
`;

const Icon = styled(IoCarSportOutline)`
  color:${(props) => props.theme.primary};
  font-size: 55px;
  margin-top: -5px;
`;
