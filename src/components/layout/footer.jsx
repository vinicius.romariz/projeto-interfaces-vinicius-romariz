import { navigate } from "@reach/router";
import { Button } from "reactstrap";
import styled from "styled-components";

const Footer = () => {
  return (
    <FooterContainer>
     <div>
        Todos os direitos Reservados a <span>SECURE CAR</span>
      </div>
      <Button onClick={() => navigate("/login")}>LOGIN</Button>
    </FooterContainer>
  );
};

export default Footer;

const FooterContainer = styled.div`
  border-top: 3px solid #03045E;
  background-color: ${(props) => props.theme.primary};
  width: 100%;
  display: flex;
  text-align: center;
  color: #fff;
  padding: 12px 10px;
  font-family: "Roboto", sans-serif;
`;
