import { Link, navigate } from "@reach/router";
import React, { useState } from "react";
import {
  Collapse,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
  UncontrolledDropdown
} from "reactstrap";
import styled from "styled-components";
import { removeToken } from "../../config/storage";
import { getUserByToken } from "../../utils/helpers";

const LayoutAdmin = ({ children }) => {
  const [user] = useState(getUserByToken() || {});
  const [openMenu, setOpenMenu] = useState(false);

  const pathDefault = "/admin";
  const defineTo = (path) => (path === "/" ? pathDefault : pathDefault + path);

  const logout = () => {
    removeToken();
    navigate("/");
  };

  return (
    <Admin>
      <Menu>
        <Container>
          <Navbar expand="md" dark>
            <NavbarBrand href="/">Administrador</NavbarBrand>
            <NavbarToggler onClick={() => setOpenMenu(!openMenu)} />
            <Nav navbar>
              <NavItem>
                <NavLink tag={Link} to={defineTo("/")}>
                  Home
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to={defineTo("/cotacoes")}>
                  Cotações
                </NavLink>
              </NavItem>
            </Nav>
            <CollapseCustom navbar isOpen={openMenu}>
              <Nav navbar>
                <NavItem>
                  <UncontrolledDropdown inNavbar nav>
                    <DropdownToggle caret nav>
                      {user.name}
                    </DropdownToggle>
                    <DropdownMenu end>
                      <DropdownItem tag={Link} to={defineTo("/perfil")}>
                        Editar Perfil
                      </DropdownItem>
                      <DropdownItem divider />
                      <DropdownItem onClick={logout}>Sair</DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </NavItem>
              </Nav>
            </CollapseCustom>
          </Navbar>
        </Container>
      </Menu>
      <Main>
        <Container>
          <main>{children}</main>
        </Container>
      </Main>
    </Admin>
  );
};

export default LayoutAdmin;

const Admin = styled.div``;
const Main = styled.div``;
const Menu = styled.div`
  background-color: ${({ theme }) => theme.primary};
`;
const CollapseCustom = styled(Collapse)`
  display: flex;
  justify-content: flex-end;
`;
