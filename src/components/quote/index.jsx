import { useEffect, useState } from "react";
import { Input, Button, Label } from "reactstrap";
import { getCities, getStates } from "../../services/stateCities.service";
import { getBrands, getModelFromBrand } from "../../services/vehicle.service";
import Quotation from "../quotation";
import {
  QuoteContainer, 
  Title,
  QuoteForm,
  FormGroup,
  LabelTitle,
  Group,
} from "./style";

const Quote = () => {
  const [form, setForm] = useState({});
  const [state] = useState(getStates());
  const [cities, setCities] = useState([]);
  const [feedBack, setFeedBack] = useState(false);
  const [formValid, setIFormValid] = useState(false);
  const [brand, setBrand] = useState([]);
  const [model, setModel] = useState([]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setForm({
      ...form,
      [name]: value,
    });
  };

  useEffect(() => {
    (async () => {
      const brands = await getBrands();
      setBrand(brands);
    })();
  }, []);

  useEffect(() => {
    if (form?.location_state) {
      const cidades = getCities(form.location_state);
      setCities(cidades);
    }
  }, [form.location_state]);

  const handleChangeBrand = async (e) => {
    try {
      const { codigo, nome } = brand.find(
        (item) => item.codigo === e.target.value
      );
      setForm({ ...form, vehicle_brand: nome });

      const models = await getModelFromBrand(codigo);
      setModel(models);
    } catch (error) {
      console.log(error);
    }
  };

  const handleSubmit = () => {
    setFeedBack(true);
  };

  useEffect(() => {
    const validateInputs = [
      "client_name",
      "client_email",
      "location_state",
      "location_city",
      "vehicle_brand",
      "vehicle_model",
    ];
    const invalidateForm = validateInputs.some((field) => !form[field]);
    setIFormValid(!invalidateForm);
  }, [form]);

  return (
    <QuoteContainer>
      <Title>Simulação de Cotação de Seguro</Title>
      <QuoteForm>
        {/* ---------------------------------------------- */}
        <LabelTitle>:: Dados do Cliente</LabelTitle>
        <FormGroup>
          <Input
            id="nome"
            name="client_name"
            placeholder="Informe seu nome"
            type="text"
            onChange={handleChange}
          />
          <Input
            id="email"
            name="client_email"
            placeholder="Informe seu email"
            type="email"
            onChange={handleChange}
          />
        </FormGroup>

        {/* ---------------------------------------------- */}
        <LabelTitle>:: Localização</LabelTitle>
        <FormGroup>
          <Group>
            <Label for="location_state">Estado:</Label>
            <Input
              id="location_state"
              name="location_state"
              type="select"
              onChange={handleChange}
            >
              <option value=""> -- UF --</option>
              {state.map((item) => (
                <option value={item.sigla} key={item.sigla}>
                  {item.sigla} - {item.nome}
                </option>
              ))}
            </Input>
          </Group>
          <Group>
            <Label for="location_city">Cidade:</Label>
            <Input
              id="location_city"
              name="location_city"
              type="select"
              onChange={handleChange}
              disabled={cities.length === 0}
            >
              <option value=""> -- Cidade --</option>
              {cities.map((item) => (
                <option key={item} value={item}>
                  {item}
                </option>
              ))}
            </Input>
          </Group>
        </FormGroup>
        {/* ---------------------------------------------- */}
        <LabelTitle>:: Dados do Veículo</LabelTitle>
        <FormGroup>
          <Input
            id="vehicle_brand"
            name="vehicle_brand"
            type="select"
            onChange={handleChangeBrand}
          >
            <option value=""> -- Marca --</option>
            {brand.map((item) => (
              <option key={item.codigo} value={item.codigo}>
                {item.nome}
              </option>
            ))}
          </Input>
          <Input
            id="vehicle_model"
            name="vehicle_model"
            type="select"
            onChange={handleChange}
            disabled={model.length === 0}
          >
            <option value=""> -- Modelo --</option>
            {model.map(({ nome, codigo }) => (
              <option key={codigo} value={codigo}>
                {nome}
              </option>
            ))}
          </Input>
        </FormGroup>
        {/* ---------------------------------------------- */}
        <Button color="primary" disabled={!formValid} onClick={handleSubmit}>
          Enviar Proposta
        </Button>
        {feedBack && <Quotation data={form} />}
      </QuoteForm>
    </QuoteContainer>
  );
};

export default Quote;
