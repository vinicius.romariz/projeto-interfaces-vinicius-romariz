import styled from "styled-components";

const Quotation = ({ data }) => {
  return (
    <QuotationContainer>
      <Item>Nome: {data?.client_name}</Item>
      <Item>Email: {data?.client_email}</Item>
      <Item>Estado: {data?.location_state}</Item>
      <Item>Cidade: {data?.location_city}</Item>
      <Item>Marca: {data?.vehicle_brand}</Item>
      {/* <Item>Modelo: {data?.vehicle_model?.nome}</Item> Não consegui resolver para mostrar a string do nome do veiculo*/}
      <Item>Número do Modelo: {data?.vehicle_model}</Item>
    </QuotationContainer>
  );
};

export default Quotation;

const QuotationContainer = styled.div`
  padding: 10px;
  border: 1px solid #ccc;
  display: flex;
  flex-direction: column;
  margin-top: 30px;
  background-color: #6998AB;
  border-radius: 5px;
`;

const Item = styled.div`
  flex: 1;
`;
