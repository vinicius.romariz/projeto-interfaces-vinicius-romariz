import styled from "styled-components";
import BannerImg from "../../assets/img/logo_banner.png";

const Banner = () => {
  return (
    <BannerContainer>
      <Title>
        <p>
          Economize até 30% em seu seguro de <span>carro</span> em minutos.
        </p>
      </Title>
      <ImgBanner src={BannerImg} alt="" />
    </BannerContainer>
  );
};

export default Banner;

const BannerContainer = styled.div`
  display: flex;
  justify-content: center;
  padding: 30px;
`;

const Title = styled.div`
  flex: 1;
  font-size: 40px;
  text-align: center;
  text-transform: uppercase;
  font-weight: 700;
  color: ${(props) => props.theme.primary};
  display: flex;
  flex-direction: column;
  justify-content: center;
  span {
    color: ${(props) => props.theme.alternative};
  }
`;

const ImgBanner = styled.img`
  height: 350px;
  flex: 1;
`;
