const { createServer, Response } = require("miragejs");

const server = createServer({
  routes() {
    this.get("http://localhost:1337/api/users", (schema, request) => {
      return [
        { id: 1, nome: "Alexandre" },
        { id: 2, nome: "Willian" },
        { id: 3, nome: "Vinicius" },
      ];
    });
    this.post("http://localhost:1337/api/auth/local", (schema, request) => {
      const { identifier, password } = JSON.parse(request.requestBody);
      if (identifier !== "contato@liniker.com.br" || password !== "12341234") {
        return new Response(
          400,
          { some: "header" },
          { errors: ["name cannot be blank"] }
        );
      }

      return {
        bananinha: "12345",
        jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjQ5MjAzNDEzLCJleHAiOjE2NTE3OTU0MTN9.q_JzdtslTZ9OUkChYMdIaRwTtVQp2P847WsL7m6SCuI",
        user: {
          id: 1,
          username: "willian",
          email: "willian@al.infnet.edu.br",
          provider: "local",
          confirmed: true,
          blocked: false,
          createdAt: "2022-04-01T23:17:44.643Z",
          updatedAt: "2022-04-01T23:37:58.462Z",
        },
      };
    });
  },
});

export default server;
