import { Router } from "@reach/router";
import React from "react";
import LayoutAdmin from "../../components/layout/admin";
// rotas de views
import Home from "./home";
import Profile from "./profile";
import Quotes from "./quotes";


const Admin = () => {
  return (
    <LayoutAdmin>
      <Router>
        <Home path="/" />
        <Quotes path="/cotacoes" />
        <Profile path="/perfil" />
      </Router>
    </LayoutAdmin>
  );
};

export default Admin;
