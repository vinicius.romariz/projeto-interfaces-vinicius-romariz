import React from "react";
import { useDispatch } from "react-redux";
import { Button, Container } from "reactstrap";
import styled from "styled-components";
import CompA from "../../components/home/compA";
import CompB from "../../components/home/compB";
const Home = () => {
  const dispatch = useDispatch();

  const incluirUsuario = () =>
    dispatch({
      type: "UPDATE_USER",
    });

  return (
    <Container>
      <h2>Lista de Estudantes</h2>
      <Box>
        <CompA />
        <CompB />
      </Box>

      <Button color="primary" onClick={incluirUsuario}>
        Incluir
      </Button>
    </Container>
  );
};

export default Home;

const Box = styled.div`
  display: flex;
  div {
    background: #eee;

    flex: 1;
    margin: 10px;
  }
`;
