import { useFormik } from "formik";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Alert, Col, Container } from "reactstrap";
import styled from "styled-components";
import * as yup from "yup";
import { Button } from "../../components/button";
import { FormField } from "../../components/formField";
import { LoginAction } from "../../store/user/action";

const Login = () => {
  const dispatch = useDispatch();
  const [error] = useState("");
  const formik = useFormik({
    initialValues: {
      email: "willian@al.infnet.edu.br",
      password: "12341234",
    },
    validationSchema: yup.object().shape({
      email: yup
        .string()
        .required("Preencha o e-mail")
        .email("Preencha um e-mail válido."),
      password: yup
        .string()
        .required("Preencha uma senha válida")
        .min(8, "Preencha senha com no mínimo 8 caracteres"),
    }),
    onSubmit: (values) => dispatch(LoginAction(values)),
    // try {
    //   setError("");
    //   const {
    //     data: { token },
    //   } = await authenticatedUser(values);
    //   http.defaults.headers["authorization"] = `${token.type} ${token.token}`;
    //   saveToken(token);
    //   navigate("/admin");
    // } catch (error) {
    //   setError("Erro ao tentar fazer o login, verique seu e-mail e/ou senha");
    // }
    // },
  });

  const getFieldProps = (fieldName) => ({
    ...formik.getFieldProps(fieldName),
    // isValid: formik.touched[fieldName] && !formik.errors[fieldName],
    // isInvalid: formik.touched[fieldName] && !!formik.errors[fieldName],
    error: formik.errors[fieldName],
  });

  return (
    <LoginContainer>
      <Col sm={12} md={6} lg={3}>
        <Form inline onSubmit={formik.handleSubmit}>
          <FormField
            {...getFieldProps("email")}
            label="E-mail"
            type="email"
            placeholder="Informe seu e-mail de acesso"
          />
          <FormField
            {...getFieldProps("password")}
            label="Senha"
            type="password"
            placeholder="Informe sua senha de acesso"
          />
          <Button
            outline
            block
            type="submit"
            className="mb-4"
            loading={formik.isValid || formik.isSubmitting}
            disabled={!formik.isValid || formik.isSubmitting}
          >
            Entrar
          </Button>
        </Form>
        {error.length > 0 && <Alert color="danger">{error}</Alert>}
      </Col>
    </LoginContainer>
  );
};

export default Login;

const Form = styled.form`
  background-color: #fff;
  padding: 20px;
`;

const LoginContainer = styled(Container)`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;
