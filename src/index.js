import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ThemeProvider } from "styled-components";
import GlobalStyle from "./config/globalStyled";
import "./config/plugins";
import { theme } from "./config/theme";
import "./mock/server";
import Toastify from "./plugins/toast";
import Routers from "./routers";
import store from "./store";


ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <Routers />
      <GlobalStyle />
    </ThemeProvider>
    <Toastify />
  </Provider>,
  document.getElementById("root")
);
